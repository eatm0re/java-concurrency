package edu.eatmore.java.concurrency;

public class Runner {

  private static final int N = 100;

  public static void main(String[] args) throws InterruptedException {
    for (int i = 0; i < N; i++) {
      ReaderThread readerThread = new ReaderThread();
      readerThread.start();
      readerThread.join();
    }
  }

}
