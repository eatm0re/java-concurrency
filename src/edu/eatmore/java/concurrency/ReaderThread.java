package edu.eatmore.java.concurrency;

public class ReaderThread extends Thread {

  private static final int TRY_COUNT = 1_000_000;

  public ReaderThread() {
    setName("ReaderThread");
  }

  @Override
  public void run() {
    try {
      WriterThread writerThread = new WriterThread();
      writerThread.start();
      //for (int i = 0; i < TRY_COUNT && writerThread.container.b == 0; i++);
      while (writerThread.container.b == 0);
      int a = writerThread.container.a;
      int b = writerThread.container.b;
      System.out.printf("[%d %d]%n", a, b);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
