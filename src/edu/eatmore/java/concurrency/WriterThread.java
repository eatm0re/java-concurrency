package edu.eatmore.java.concurrency;

public class WriterThread extends Thread {

  public Container container = new Container();

  public WriterThread() {
    setName("WriterThread");
  }

  @Override
  public void run() {
    container.a = 1;
    container.b = 1;
  }

}
